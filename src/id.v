`include "defines.v"
`include "opcodes.v"

module id(
	input wire rst,
	input wire[`InstAddrBus] pc_i,
	input wire[`InstBus] inst_i,

	input wire[`RegBus] reg1_data_i,
	input wire[`RegBus] reg2_data_i,

	input wire[`AluOpBus] ex_aluop_i, 
	input wire					ex_wreg_i,
	input wire[`RegBus]			ex_wdata_i,
	input wire[`RegAddrBus]     ex_wd_i,
	
	input wire					mem_wreg_i,
	input wire[`RegBus]			mem_wdata_i,
	input wire[`RegAddrBus]     mem_wd_i,

	output reg reg1_read_o,
	output reg reg2_read_o,
	output reg [`RegAddrBus] reg1_addr_o,
	output reg [`RegAddrBus] reg2_addr_o,

	output reg [`AluOpBus] aluop_o,
	output reg [`AluFunct3Bus] alufunct3_o,
	output reg [`AluFunct7Bus] alufunct7_o,
	output reg [`RegBus] reg1_o,
	output reg [`RegBus] reg2_o,
	output reg [`RegAddrBus] wd_o,
	output reg wreg_o,

	output wire    stallreq,

	output reg branch_flag_o,
	output reg[`RegBus] branch_target_address_o,

	output wire[`RegBus] inst_o
);

reg [`RegBus] imm;

wire [`AluOpBus] op_o = inst_i[6:0];
wire [`AluFunct7Bus] funct7_o = inst_i[31:25];
wire [`AluFunct3Bus] funct3_o = inst_i[14:12];
reg stallreq_for_reg1_loadrelate;
reg stallreq_for_reg2_loadrelate;
wire pre_inst_is_load;

assign pre_inst_is_load = (ex_aluop_i == `LOAD);
assign stallreq = stallreq_for_reg1_loadrelate | stallreq_for_reg2_loadrelate;

assign inst_o = inst_i;

always @(*) begin
if(rst == `RstEnable) begin
	aluop_o <= `EXE_NOP_OP;
	alufunct7_o <= `EXE_NOP_OP;
	alufunct3_o <= `EXE_FUNCT_NOP;
	wd_o <= `NOPRegAddr;
	wreg_o <= `WriteDisable;
	reg1_read_o <= 1'b0;
	reg2_read_o <= 1'b0;
	reg1_addr_o <= `NOPRegAddr;
	reg2_addr_o <= `NOPRegAddr;
	imm <= 32'h0;	
	branch_target_address_o <= `ZeroWord;
	branch_flag_o <= `NotBranch;
end
else begin
	wd_o <= inst_i[11:7];
	reg1_addr_o <= inst_i[19:15];
	reg2_addr_o <= inst_i[24:20];
	alufunct3_o <= funct3_o;
	alufunct7_o <= funct7_o;
	aluop_o <= op_o;
	wreg_o <= `WriteDisable;
	branch_flag_o <= `NotBranch;
	branch_target_address_o <= `ZeroWord;
	case(op_o)
		`LUI: begin
			aluop_o <= `LUI;
			wreg_o <= 1'b1;
			reg1_read_o <= 1'b0;
			imm[31:0] <= {{inst_i[31:12]}, 12'h0};
			reg2_read_o <= 1'b0;
		end
		`AUIPC: begin
			aluop_o <= `AUIPC;
			wreg_o <= 1'b1;
			reg1_read_o <= 1'b0;
			imm[31:0] <= {{inst_i[31:12]}, 12'h0} + pc_i;
			reg2_read_o <= 1'b0;
		end
		`JAL: begin
			aluop_o <= `JAL;
			wreg_o <= `WriteEnable;
			reg1_read_o <= 1'b0;
			reg2_read_o <= 1'b0;
			imm <= pc_i + 4'h4;
			branch_flag_o <= `Branch;
			branch_target_address_o <= pc_i + {{12{inst_i[31]}}, {inst_i[19:12]}, {inst_i[20]}, {inst_i[30:21]}, {1'h0}};

		end
		`JALR: begin
			aluop_o <= `JALR;
			wreg_o <= `WriteEnable;
			reg1_read_o <= 1'b1;
			reg2_read_o <= 1'b0;
			imm <= pc_i + 4'h4;
			branch_flag_o <= `Branch;
			branch_target_address_o <= reg1_o + {{21{inst_i[31]}}, {inst_i[30:20]}};
		end
		`BRANCH: begin
			aluop_o <= `BRANCH;
			wreg_o <= `WriteDisable;
			case(funct3_o)
				`BEQ: begin
					reg1_read_o <= 1'b1;
					reg2_read_o <= 1'b1;
					if(reg1_o == reg2_o) begin
						branch_target_address_o <= pc_i + {{20{inst_i[31]}}, {inst_i[7]}, {inst_i[30:25]}, {inst_i[11:8]}, {1'h0}};
						branch_flag_o <= `Branch;
					end
				end
				`BNE: begin
					reg1_read_o <= 1'b1;
					reg2_read_o <= 1'b1;
					if(reg1_o != reg2_o) begin
						branch_target_address_o <= pc_i + {{20{inst_i[31]}}, {inst_i[7]}, {inst_i[30:25]}, {inst_i[11:8]}, {1'h0}};
						branch_flag_o <= `Branch;
					end
				end
				`BLT: begin
					reg1_read_o <= 1'b1;
					reg2_read_o <= 1'b1;
					if(($signed(reg1_o) < $signed(reg2_o))) begin
						branch_target_address_o <= pc_i + {{20{inst_i[31]}}, {inst_i[7]}, {inst_i[30:25]}, {inst_i[11:8]}, {1'h0}};
						branch_flag_o <= `Branch;
					end
				end
				`BGE: begin
					reg1_read_o <= 1'b1;
					reg2_read_o <= 1'b1;
					if(!($signed(reg1_o) < $signed(reg2_o))) begin
						branch_target_address_o <= pc_i + {{20{inst_i[31]}}, {inst_i[7]}, {inst_i[30:25]}, {inst_i[11:8]}, {1'h0}};
						branch_flag_o <= `Branch;
					end
				end
				`BLTU: begin
					reg1_read_o <= 1'b1;
					reg2_read_o <= 1'b1;
					if(reg1_o < reg2_o) begin
						branch_target_address_o <= pc_i + {{20{inst_i[31]}}, {inst_i[7]}, {inst_i[30:25]}, {inst_i[11:8]}, {1'h0}};
						branch_flag_o <= `Branch;
					end
				end
				`BGEU: begin
					reg1_read_o <= 1'b1;
					reg2_read_o <= 1'b1;
					if(!(reg1_o < reg2_o)) begin
						branch_target_address_o <= pc_i + {{20{inst_i[31]}}, {inst_i[7]}, {inst_i[30:25]}, {inst_i[11:8]}, {1'h0}};
						branch_flag_o <= `Branch;
					end
				end
			endcase
		end
		`LOAD: begin
			aluop_o <= `LOAD;
			wreg_o <= `WriteEnable;
			reg1_read_o <= 1'b1;
			reg2_read_o <= 1'b0;
			imm[31:0] <= {{21{inst_i[31]}},inst_i[30:20]};
		end
		`STORE: begin
			aluop_o <= `STORE;
			wreg_o <= `WriteDisable;
			reg1_read_o <= 1'b1;
			reg2_read_o <= 1'b1;
		end
		`IMM: begin
			aluop_o <= `IMM;
			wreg_o <= `WriteEnable;
			reg1_read_o <= 1'b1;
			reg2_read_o <= 1'b0;
			imm[31:0] <= {{21{inst_i[31]}},inst_i[30:20]};
			// case (funct3_o)
			// 	`ADDI: begin
			// 		alufunct3_o <= `ADDI;
			// 	end
			// 	`SLTI : begin
			// 		alufunct3_o <= `SLTI;		
			// 	end
			// 	`SLTIU : begin
			// 		alufunct3_o <= `SLTIU;		
			// 	end
			// 	`XORI : begin
			// 		alufunct3_o <= `XORI;
			// 	end
			// 	`ORI : begin
			// 		alufunct3_o <= `ORI;	
			// 	end
			// 	`ANDI : begin
			// 		alufunct3_o <= `ANDI;
			// 	end
			// endcase
		end
		`ALU: begin
			wreg_o <= `WriteEnable;
			reg1_read_o <= 1'b1;
			reg2_read_o <= 1'b1;
		end
		`FENCE: begin
			
		end
	endcase
end
end
always @ (*) begin
	stallreq_for_reg1_loadrelate <= `NoStop;
	if(rst == `RstEnable) begin
			reg1_o <= `ZeroWord;
	  end else if(pre_inst_is_load == 1'b1 && ex_wd_i == reg1_addr_o 
								&& reg1_read_o == 1'b1 ) begin
		stallreq_for_reg1_loadrelate <= `Stop;
	  end else if((reg1_read_o == 1'b1) && (reg1_addr_o == 5'b0)) begin
	  	reg1_o <= 32'h0;
	  end else if((reg1_read_o == 1'b1) && (ex_wreg_i == 1'b1) 
	  							&& (ex_wd_i == reg1_addr_o)) begin
	  	reg1_o <= ex_wdata_i;
	  end else if((reg1_read_o == 1'b1) && (mem_wreg_i == 1'b1) 
								&& (mem_wd_i == reg1_addr_o)) begin
		reg1_o <= mem_wdata_i;
	  end else if(reg1_read_o == 1'b1) begin
	  	reg1_o <= reg1_data_i;
	  end else if(reg1_read_o == 1'b0) begin
	  	reg1_o <= imm;
	  end else begin
	    reg1_o <= `ZeroWord;
	  end
	end
	
	always @ (*) begin
		stallreq_for_reg2_loadrelate <= `NoStop;
		if(rst == `RstEnable) begin
			reg2_o <= `ZeroWord;
		end else if(pre_inst_is_load == 1'b1 && ex_wd_i == reg2_addr_o 
								&& reg2_read_o == 1'b1 ) begin
		  stallreq_for_reg2_loadrelate <= `Stop;
		end else if((reg2_read_o == 1'b1) && (reg2_addr_o == 5'b0)) begin 
	  		reg1_o <= 32'h0;
		end else if((reg2_read_o == 1'b1) && (ex_wreg_i == 1'b1) 
								&& (ex_wd_i == reg2_addr_o)) begin
			reg2_o <= ex_wdata_i; 
		end else if((reg2_read_o == 1'b1) && (mem_wreg_i == 1'b1) 
								&& (mem_wd_i == reg2_addr_o)) begin
			reg2_o <= mem_wdata_i;
	  end else if(reg2_read_o == 1'b1) begin
	  	reg2_o <= reg2_data_i;
	  end else if(reg2_read_o == 1'b0) begin
	  	reg2_o <= imm;
	  end else begin
	    reg2_o <= `ZeroWord;
	  end
	end
endmodule