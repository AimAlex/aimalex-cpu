`include "defines.v"
module id_ex(
	input wire clk,
	input wire rst,
	input wire jrst,
	

	input wire id_branch_flag,
	input wire[`AluOpBus] id_aluop,
	input wire[`AluFunct3Bus] id_alufunct3,
	input wire[`AluFunct7Bus] id_alufunct7,
	input wire[`RegBus] id_reg1,
	input wire[`RegBus] id_reg2,
	input wire[`RegAddrBus] id_wd,
	input wire id_wreg,
	input wire [`RegBus] id_inst,

	input wire[5:0] stall,


	output reg[`AluOpBus] ex_aluop,
	output reg[`AluFunct3Bus] ex_alufunct3,
	output reg[`AluFunct7Bus] ex_alufunct7,
	output reg[`RegBus] ex_reg1,
	output reg[`RegBus] ex_reg2,
	output reg[`RegAddrBus] ex_wd,
	output reg ex_wreg,
	output reg [`RegBus] ex_inst,
	output reg ex_branch_flag

);
	always @(posedge clk) begin
		if (rst == `RstEnable || jrst == `RstEnable) begin
			ex_aluop <= `EXE_NOP_OP;
			ex_alufunct3 <= `EXE_FUNCT_NOP;
			ex_alufunct7 <= `EXE_NOP_OP;
			ex_reg1 <= `ZeroWord;
			ex_reg2 <= `ZeroWord;
			ex_wd <= `NOPRegAddr;
			ex_wreg <= `WriteDisable;
			ex_inst <= `ZeroWord;
			ex_branch_flag <= `NotBranch;
		end
		else if(stall[2] == `Stop && stall[3] == `NoStop) begin
			ex_aluop <= `EXE_NOP_OP;
			ex_alufunct3 <= `EXE_FUNCT_NOP;
			ex_alufunct7 <= `EXE_NOP_OP;
			ex_reg1 <= `ZeroWord;
			ex_reg2 <= `ZeroWord;
			ex_wd <= `NOPRegAddr;
			ex_wreg <= `WriteDisable;
			ex_inst <= `ZeroWord;
			ex_branch_flag <= `NotBranch;
		end
		else if(stall[2] == `NoStop)begin
		//$display("%b idresult(%h) <- %h", ex_aluop, ex_reg1, ex_reg2);
			ex_aluop <= id_aluop;
			ex_alufunct3 <= id_alufunct3;
			ex_alufunct7 <= id_alufunct7;
			ex_reg1 <= id_reg1;
			ex_reg2 <= id_reg2;
			ex_wd <= id_wd;
			ex_wreg <= id_wreg;
			ex_inst <= id_inst;
			ex_branch_flag <= id_branch_flag;
		end
	end
endmodule