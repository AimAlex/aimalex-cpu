`include "defines.v"

module min_sopc (
	input wire clk,
	input wire rst
);

	wire [`InstAddrBus] inst_addr;
	wire [`InstBus] inst;
	wire rom_ce;
	wire mem_we_i;
	wire [`RegBus] mem_addr_i;
	wire [`RegBus] mem_data_i;
	wire [`RegBus] mem_data_o;
	wire [3:0] mem_sel_i;
	wire mem_ce_i;

	wire data_ram_vce;
	wire[`ByteWidth] data_ram_get0;
	wire[`ByteWidth] data_ram_get1;
	wire[`ByteWidth] data_ram_get2;
	wire[`ByteWidth] data_ram_get3;
	wire[`ByteWidth] data_ram_get4;
	wire[`ByteWidth] data_ram_get5;
	wire[`ByteWidth] data_ram_get6;
	wire[`ByteWidth] data_ram_get7;
	wire[`DataBus] cache_data;
	wire rce;
	wire rwe;
	wire[3:0] rsel;
	wire [`DataAddrBus] raddr;
	wire stal;
	wire [`DataAddrBus] caddr;


	cpu cpu0 (
		.clk(clk), .rst(rst),
		.rom_data_i(inst),

		
		.rom_addr_o(inst_addr), 		
		.rom_ce_o(rom_ce),

		.mem_stal(stal),

		.ram_we_o(mem_we_i),
		.ram_addr_o(mem_addr_i),
		.ram_sel_o(mem_sel_i),
		.ram_data_o(mem_data_i),
		.ram_data_i(mem_data_o),
		.ram_ce_o(mem_ce_i)
	);

	inst_rom inst_rom0(
		.ce (rom_ce),
		.addr(inst_addr),

		.inst(inst)
	);

	data_ram data_ram0(
		.clk(clk),
		.rce(rce),
		.rwe(rwe),
		.rsel(rsel),
		.raddr(raddr),
		.data_i(cache_data),
		
		.vce(data_ram_vce),
		.cache_get0(data_ram_get0),
		.cache_get1(data_ram_get1),
		.cache_get2(data_ram_get2),
		.cache_get3(data_ram_get3),
		.cache_get4(data_ram_get4),
		.cache_get5(data_ram_get5),
		.cache_get6(data_ram_get6),
		.cache_get7(data_ram_get7),
		.caddr(caddr)
	);

	cache cache0(

		.clk(clk),
		.ce(mem_ce_i),
		.we(mem_we_i),
		.addr(mem_addr_i),
		.sel(mem_sel_i),
		.data_i(mem_data_i),
		.vce(data_ram_vce),
		.cache_get0(data_ram_get0),
		.cache_get1(data_ram_get1),
		.cache_get2(data_ram_get2),
		.cache_get3(data_ram_get3),
		.cache_get4(data_ram_get4),
		.cache_get5(data_ram_get5),
		.cache_get6(data_ram_get6),
		.cache_get7(data_ram_get7),
		.caddr(caddr),

		.data_o(mem_data_o),
		.ram_data(cache_data),
		.rce(rce),
		.rwe(rwe),
		.rsel(rsel),
		.raddr(raddr),
		.stal(stal)


	);
endmodule