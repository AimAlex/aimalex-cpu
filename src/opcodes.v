`define BRANCH 7'b1100011
`define LOAD   7'b0000011
`define STORE  7'b0100011
`define JALR   7'b1100111
`define JAL    7'b1101111
`define FENCE  7'b0001111
`define IMM    7'b0010011
`define OP     7'b0010011
`define ALU    7'b0110011 
`define LUI    7'b0110111
`define AUIPC  7'b0001111

`define BEQ 3'b000
`define BNE 3'b001
`define BLT 3'b100
`define BGE 3'b101
`define BLTU 3'b110
`define BGEU 3'b111

`define LB 3'b000
`define LH 3'b001
`define LW 3'b010
`define LBU 3'b100
`define LHU 3'b101

`define SB 3'b000
`define SH 3'b001
`define SW 3'b010

`define ADDI 3'b000
`define SLTI 3'b010
`define SLTIU 3'b011
`define XORI 3'b100
`define ORI 3'b110
`define ANDI 3'b111

`define funct3_SLLI 3'b001
`define funct3_SRLI 3'b101
`define funct3_SRAI 3'b101
`define funct3_ADD 3'b000
`define funct3_SUB 3'b000
`define funct3_SLL 3'b001
`define funct3_SLT 3'b010
`define funct3_SLTU 3'b011
`define funct3_XOR 3'b100
`define funct3_SRL 3'b101
`define funct3_SRA 3'b101
`define funct3_OR 3'b110
`define funct3_AND 3'b111

`define funct7_SLLI 7'b0000000
`define funct7_SRLI 7'b0000000
`define funct7_SRAI 7'b0100000
`define funct7_ADD 7'b0000000
`define funct7_SUB 7'b0100000
`define funct7_SLL 7'b0000000
`define funct7_SLT 7'b0000000
`define funct7_SLTU 7'b0000000
`define funct7_XOR 7'b0000000
`define funct7_SRL 7'b0000000
`define funct7_SRA 7'b0100000
`define funct7_OR 7'b0000000
`define funct7_AND 7'b0000000

`define funct3_FENCE 3'b000
`define funct3_FENCEI 3'b001