`include "defines.v"

module cache(

	input wire  clk,
	input wire	ce,
	input wire	we,
	input wire[`DataAddrBus]	addr,
	input wire[3:0] sel,
	input wire[`DataBus]	data_i,
	input wire vce,//ram read
	input wire[`ByteWidth] cache_get0,
	input wire[`ByteWidth] cache_get1,
	input wire[`ByteWidth] cache_get2,
	input wire[`ByteWidth] cache_get3,
	input wire[`ByteWidth] cache_get4,
	input wire[`ByteWidth] cache_get5,
	input wire[`ByteWidth] cache_get6,
	input wire[`ByteWidth] cache_get7,
	input wire[`DataAddrBus] caddr,
	//input wire 


	output reg[`DataBus]	data_o,
	output reg[`DataBus] ram_data,
	output reg rce,//读写ram
	output reg rwe,//读或写
	output reg[3:0] rsel,
	output reg[`DataAddrBus] raddr,
	output reg stal
);	
	reg [`CacheMemNumLog2] cache_head;
	reg [`CacheMemNumLog2] cache_tail; 
	//reg cache_dirty [0:`CacheMemNum-1];
	reg cache_valid [0:`CacheMemNum-1];
	reg [`DataAddrBus] cache_tag[0:`CacheMemNum-1]; 
	reg [`ByteWidth]  cache_mem0[0:`CacheMemNum-1];
	reg [`ByteWidth]  cache_mem1[0:`CacheMemNum-1];
	reg [`ByteWidth]  cache_mem2[0:`CacheMemNum-1];
	reg [`ByteWidth]  cache_mem3[0:`CacheMemNum-1];
	reg [`ByteWidth]  cache_mem4[0:`CacheMemNum-1];
	reg [`ByteWidth]  cache_mem5[0:`CacheMemNum-1];
	reg [`ByteWidth]  cache_mem6[0:`CacheMemNum-1];
	reg [`ByteWidth]  cache_mem7[0:`CacheMemNum-1];
	reg [`CacheMemNum] i;

	initial begin
	   for(i = 0; i < `CacheMemNum; i = i + 1) begin
	       //cache_dirty[i] = 1'b0;
           cache_valid[i] = 1'b0;
           cache_tag[i] = 31'h0;
	   end
	end

	initial begin
		cache_head = `CacheMemNumLog2'b0;
		cache_tail = `CacheMemNumLog2'b0;
		stal = 1'b0;
		rce = 1'b0;
	end


	always @(posedge clk) begin
		if (vce == 1'b1) begin
			if(!(cache_head == cache_tail) || cache_valid[cache_tail] == 1'b0) begin
				cache_valid[cache_tail] = 1'b1;
				cache_tag[cache_tail] = caddr;
				cache_mem0[cache_tail] = cache_get0;
				cache_mem1[cache_tail] = cache_get1;
				cache_mem2[cache_tail] = cache_get2;
				cache_mem3[cache_tail] = cache_get3;
				cache_mem4[cache_tail] = cache_get4;
				cache_mem5[cache_tail] = cache_get5;
				cache_mem6[cache_tail] = cache_get6;
				cache_mem7[cache_tail] = cache_get7;
				cache_valid[cache_tail] = 1'b1;
				cache_tail = (cache_tail + 1) % `CacheMemNum;
			end
			else begin
				cache_tag[cache_tail] = caddr;
				cache_mem0[cache_tail] = cache_get0;
				cache_mem1[cache_tail] = cache_get1;
				cache_mem2[cache_tail] = cache_get2;
				cache_mem3[cache_tail] = cache_get3;
				cache_mem4[cache_tail] = cache_get4;
				cache_mem5[cache_tail] = cache_get5;
				cache_mem6[cache_tail] = cache_get6;
				cache_mem7[cache_tail] = cache_get7;
				cache_valid[cache_tail] = 1'b1;
				cache_tail = (cache_tail + 1) % `CacheMemNum;
			end
			
			//cache_valid[cache_tail] = 1'b1;
			//$display("%d writecache(%d) <- %h", cache_tail, addr, cache_get0);
			stal = 1'b0;
		end
		
	end
	always @ (posedge clk) begin
		$display("cache0 <- %h",cache_mem0[0]);
		$display("cache1 <- %h",cache_mem1[0]);
		$display("cache2 <- %h",cache_mem2[0]);
		$display("cache3 <- %h",cache_mem3[0]);
		$display("cache4 <- %h",cache_mem4[0]);
		$display("cache5 <- %h",cache_mem5[0]);
		$display("cache6 <- %h",cache_mem6[0]);
		$display("cache7 <- %h",cache_mem7[0]);
		if (ce == `ChipDisable) begin
			//data_o <= ZeroWord;
		end else if(we == `WriteEnable) begin
			rce = 1'b1;
			for(i = 0; i < `CacheMemNum; i = i + 1) begin
				if(addr[`DataMemNumLog2+1:3] ==  cache_tag[i][`DataMemNumLog2+1:3] && cache_valid[i] == 1'b1 ) begin
					//cache_dirty[i] <= 1'b1;
					rce = 1'b0;
					
					case(addr[2])
						1'b0: begin
							if (sel[3] == 1'b1) begin
		    					cache_mem3[i] = data_i[31:24];
		    				end
							if (sel[2] == 1'b1) begin
		    					cache_mem2[i] = data_i[23:16];
		    				end
		    				if (sel[1] == 1'b1) begin
		    					cache_mem1[i] = data_i[15:8];
		    				end
							if (sel[0] == 1'b1) begin
		    					cache_mem0[i] = data_i[7:0];
		    				end
						end
						1'b1: begin
							if (sel[3] == 1'b1) begin
		    					cache_mem7[i] = data_i[31:24];
		    				end
							if (sel[2] == 1'b1) begin
		    					cache_mem6[i] = data_i[23:16];
		    				end
		    				if (sel[1] == 1'b1) begin
		    					cache_mem5[i] = data_i[15:8];
		    				end
							if (sel[0] == 1'b1) begin
		    					cache_mem4[i] = data_i[7:0];
		    				end
						end
					endcase
					
				end	
			end		   	    
		end
	end
	
	always @ (posedge clk) begin
		if (ce == `ChipDisable) begin
			data_o = `ZeroWord;
	    end
	    else if(we == `WriteDisable && vce == 1'b1 && 
	    	caddr[`DataMemNumLog2+1:3] ==  addr[`DataMemNumLog2+1:3]) begin
	    		rce = 1'b0;
	    		case(addr[2]) 
						1'b0: begin 
		    				data_o = {cache_get3,
		              	 			   cache_get2,
		               				   cache_get1,
		               				   cache_get0};
		               	end
		               	1'b1: begin
		               		data_o = {cache_get7,
		              	 			   cache_get6,
		               				   cache_get5,
		               				   cache_get4};
		               	end
		            endcase
	    	end 
	   	else if(we == `WriteDisable) begin
	   		rce = 1'b1;
	    	for(i = 0; i < `CacheMemNum; i = i + 1) begin
				if(addr[`DataMemNumLog2+1:3] ==  cache_tag[i][`DataMemNumLog2+1:3] && cache_valid[i] == 1'b1) begin
					rce = 1'b0;
					case(addr[2]) 
						1'b0: begin 
		    				data_o = {cache_mem3[i],
		              	 			   cache_mem2[i],
		               				   cache_mem1[i],
		               				   cache_mem0[i]};
		               	end
		               	1'b1: begin
		               		data_o = {cache_mem7[i],
		              	 			   cache_mem6[i],
		               				   cache_mem5[i],
		               				   cache_mem4[i]};
		               	end
		            endcase
		        end
		    end
		end
		else begin
				data_o = `ZeroWord;
		end
	end	
	always @(*) begin
		raddr = addr;
		rwe = we;
		rsel = sel;
		stal = rce;
		ram_data = data_i;
		if (ce == `ChipDisable) begin
			stal = 1'b0;
		end
		// if(we == `WriteEnable && rce == 1'b1) begin
		// 	stal = 1'b1;
		// end
		// else if (rce == 1'b1) begin
		// 	stal <= 1'b1;
		// end
	end	
	// always @(posedge clk ) begin
	// 	$display("%d cache(%d) <- %h", rwe, stal, raddr);
	// end

endmodule