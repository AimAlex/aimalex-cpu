`include "defines.v"
module ex_mem(

	input wire clk,
	input wire rst,

	input wire[`RegAddrBus] ex_wd,
	input wire ex_wreg,
	input wire [`RegBus] ex_wdata,
	input wire[5:0] stall,

	input wire [`AluOpBus] ex_aluop,
	input wire [`AluFunct3Bus] ex_alufunct3,
	input wire [`RegBus] ex_mem_addr,
	input wire [`RegBus] ex_reg2,
	//input wire mem_stal,

	output reg[`RegAddrBus] mem_wd,
	output reg mem_wreg,
	output reg[`RegBus] mem_wdata,

	output reg[`AluOpBus] mem_aluop,
	output reg[`AluFunct3Bus] mem_alufunct3,
	output reg[`RegBus] mem_mem_addr,
	output reg[`RegBus] mem_reg2
);
	
	always @(posedge clk) begin
		if (rst == `RstEnable) begin
			mem_wd <= `NOPRegAddr;
			mem_wreg <= `WriteDisable;
			mem_wdata <= `ZeroWord;
			mem_aluop <= `EXE_NOP_OP;
			mem_alufunct3 <= `EXE_FUNCT_NOP;
			mem_mem_addr <= `ZeroWord;
			mem_reg2 <= `ZeroWord;
		end
		else if(stall[3] == `Stop && stall[4] == `NoStop) begin
			mem_wd <= `NOPRegAddr;
			mem_wreg <= `WriteDisable;
			mem_wdata <= `ZeroWord;
			mem_aluop <= `EXE_NOP_OP;
			mem_alufunct3 <= `EXE_FUNCT_NOP;
			mem_mem_addr <= `ZeroWord;
			mem_reg2 <= `ZeroWord;
		end
		// else if(mem_stal == 1'b1) begin
		// 	// mem_wd <= `NOPRegAddr;
		// 	// mem_wreg <= `WriteDisable;
		// 	// mem_wdata <= `ZeroWord;
		// 	// mem_aluop <= `EXE_NOP_OP;
		// 	// mem_alufunct3 <= `EXE_FUNCT_NOP;
		// 	// mem_mem_addr <= `ZeroWord;
		// 	// mem_reg2 <= `ZeroWord;
		// end
		else if(stall[3] == `NoStop) begin
		// $display("%b exresult(%b) <- %h", ex_wreg, ex_wd, ex_wdata);
			mem_wd <= ex_wd;
			mem_wreg <= ex_wreg;
			mem_wdata <= ex_wdata;
			mem_aluop <= ex_aluop;
			mem_alufunct3 <= ex_alufunct3;
			mem_mem_addr <= ex_mem_addr;
			mem_reg2 <= ex_reg2;
		end
	end

endmodule