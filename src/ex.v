`include "defines.v"
`include "opcodes.v"

module ex(

	input wire rst,

	input wire[`AluOpBus] aluop_i,
	input wire[`AluFunct3Bus] alufunct3_i,
	input wire[`AluFunct7Bus] alufunct7_i,
	input wire[`RegBus] reg1_i,
	input wire[`RegBus] reg2_i,
	input wire[`RegAddrBus] wd_i,
	input wire wreg_i,
	input wire[`RegBus] inst_i,
	input wire branch_flag_i,

	output reg[`RegAddrBus] wd_o,
	output reg wreg_o,
	output reg[`RegBus] wdata_o,
	
	output wire[`AluOpBus] aluop_o,
	output wire[`AluFunct3Bus] alufunct3_o,
	output reg[`RegBus] mem_addr_o,
	output reg[`RegBus] reg2_o,

	output wire	stallreq,

	output reg jrst
);

	reg[`RegBus] logicout;
	assign stallreq = `NoStop;
	assign aluop_o = aluop_i;
	assign alufunct3_o = alufunct3_i;
	always @ (*) begin
		if(rst == `RstEnable) begin
			logicout <= `ZeroWord;
			mem_addr_o <= `ZeroWord;
			reg2_o <= `ZeroWord;
		end 
		else begin
			mem_addr_o <= `ZeroWord;
			reg2_o <= `ZeroWord;
			jrst <= `RstDisable;
			//$display("%b opcodes(%h) <- %h", aluop_i, reg1_i, reg2_i);
			case (aluop_i)
				`LUI: begin
					logicout <= reg1_i;
				end
				`AUIPC: begin
					logicout <= reg1_i;
				end
				`JAL: begin
					logicout <= reg1_i;
					if(branch_flag_i == `Branch) begin
						jrst <= `RstEnable;
					end
					
				end
				`JALR: begin
					logicout <= reg2_i;
					if(branch_flag_i == `Branch) begin
						jrst <= `RstEnable;
					end
				end
				`BRANCH: begin
					logicout <= `ZeroWord;
					if(branch_flag_i == `Branch) begin
						jrst <= `RstEnable;
					end
				end
				`LOAD: begin
					logicout <= `ZeroWord;
					mem_addr_o <= reg1_i + reg2_i;
				end
				`STORE: begin
					logicout <= `ZeroWord;
					mem_addr_o <= reg1_i + {{21{inst_i[31]}}, {inst_i[30:25]}, {inst_i[11:7]}};
					reg2_o <= reg2_i;
				end
				`IMM: begin
					case (alufunct3_i)
						`ADDI: begin
							logicout <= reg1_i + reg2_i;
						end
						`SLTI : begin
							logicout <= ($signed(reg1_i) < $signed(reg2_i));
						end
						`SLTIU : begin
							logicout <= reg1_i < reg2_i;
						end
						`XORI : begin
							logicout <= reg1_i ^ reg2_i;
						end
						`ORI : begin
							logicout <= reg1_i | reg2_i;

						end
						`ANDI : begin
							logicout <= reg1_i & reg2_i;
						end
						`funct3_SLLI : begin
							logicout <= reg1_i << reg2_i[4:0];
						end 
						`funct3_SRLI : begin
							case (alufunct7_i)
								`funct7_SRLI : begin
									logicout <= reg1_i >> reg2_i[4:0];
								end
								`funct7_SRAI : begin
									logicout <= ({32{reg1_i[31]}} << (6'd32 - {1'b0, reg2_i[4:0]}))
												| reg1_i >> reg2_i[4:0];
								end
							endcase
						end
					endcase
				end
				`ALU: begin
					case (alufunct3_i)
						`funct3_ADD : begin
							case (alufunct7_i)
								`funct7_ADD : begin
									logicout <= reg1_i + reg2_i;
								end
								`funct7_SUB : begin
									logicout <= reg1_i - reg2_i;
								end
							endcase
						end
						`funct3_SLL : begin
							logicout <= reg1_i << reg2_i[4:0];
						end
						`funct3_SLT : begin
							logicout <= ($signed(reg1_i) < $signed(reg2_i));
						end
						`funct3_SLTU : begin
							logicout <= reg1_i < reg2_i;
						end
						`funct3_XOR : begin
							logicout <= reg1_i ^ reg2_i;
						end
						`funct3_SRL : begin
							case (alufunct7_i) 
								`funct7_SRL : begin
									logicout <= reg1_i >> reg2_i[4:0];
								end
								`funct7_SRA : begin
									logicout <= ({32{reg1_i[31]}} << (6'd32 - {1'b0, reg2_i[4:0]}))
												| reg1_i >> reg2_i[4:0];
								end
							endcase
						end
						`funct3_OR : begin
							logicout <= reg1_i | reg2_i;
						end
						`funct3_AND : begin
							logicout <= reg1_i & reg2_i;
						end
					endcase
				end
				`FENCE: begin
			
				end
			endcase
		end    //if
	end      //always


 always @ (*) begin
	wd_o <= wd_i;	 	 	
	wreg_o <= wreg_i;
	if(rst == `RstEnable) begin
		wdata_o <= `ZeroWord;
	end 
	else begin
		wdata_o <= logicout;
	end

 end	

endmodule