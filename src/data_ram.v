`include "defines.v"

module data_ram(

	input	wire    clk,
	input   wire  rce,//读写ram
	input   wire  rwe,//读或写
	input   wire [3:0] rsel,
	input   wire [`DataAddrBus] raddr,
	input   wire[`DataBus]	data_i,
	output  reg  vce,//ram read
	output  reg [`ByteWidth]  cache_get0,
	output  reg [`ByteWidth]  cache_get1,
	output  reg [`ByteWidth]  cache_get2,
	output  reg [`ByteWidth]  cache_get3,
	output  reg [`ByteWidth]  cache_get4,
	output  reg [`ByteWidth]  cache_get5,
	output  reg [`ByteWidth]  cache_get6,
	output  reg [`ByteWidth]  cache_get7,
	output  reg [`DataAddrBus] caddr
);

	reg[`ByteWidth]  data_mem0[0:`DataMemNum-1];
	reg[`ByteWidth]  data_mem1[0:`DataMemNum-1];
	reg[`ByteWidth]  data_mem2[0:`DataMemNum-1];
	reg[`ByteWidth]  data_mem3[0:`DataMemNum-1];

	always @ (posedge clk) begin
		if (rce == `ChipDisable && rwe == `WriteDisable) begin
			//data_o <= ZeroWord;
		end else if(rwe == `WriteEnable) begin
			  if (rsel[3] == 1'b1) begin
		      data_mem3[raddr[`DataMemNumLog2+1:2]] <= data_i[31:24];
		    end
			  if (rsel[2] == 1'b1) begin
		      data_mem2[raddr[`DataMemNumLog2+1:2]] <= data_i[23:16];
		    end
		    if (rsel[1] == 1'b1) begin
		      data_mem1[raddr[`DataMemNumLog2+1:2]] <= data_i[15:8];
		    end
			  if (rsel[0] == 1'b1) begin
		      data_mem0[raddr[`DataMemNumLog2+1:2]] <= data_i[7:0];
		    end			   	    
		end
	end
	
	always @ (*) begin
		vce <= 1'b0;
		caddr <= raddr;
		if (rce == `ChipDisable && rwe == `WriteDisable) begin
			//data_o <= `ZeroWord;
	  end else if(rce == `ChipEnable && rwe == `WriteEnable) begin
	  		cache_get0 <= data_mem0[{raddr[`DataMemNumLog2+1:3], 1'b0}];
	  		cache_get1 <= data_mem1[{raddr[`DataMemNumLog2+1:3], 1'b0}];
	  		cache_get2 <= data_mem2[{raddr[`DataMemNumLog2+1:3], 1'b0}];
	  		cache_get3 <= data_mem3[{raddr[`DataMemNumLog2+1:3], 1'b0}];
	  		cache_get4 <= data_mem0[{raddr[`DataMemNumLog2+1:3], 1'b1}];
	  		cache_get5 <= data_mem1[{raddr[`DataMemNumLog2+1:3], 1'b1}];
	  		cache_get6 <= data_mem2[{raddr[`DataMemNumLog2+1:3], 1'b1}];
	  		cache_get7 <= data_mem3[{raddr[`DataMemNumLog2+1:3], 1'b1}];
	  		case (raddr[2])
	  		    1'b0: begin 
	  		if (rsel[3] == 1'b1) begin
		      cache_get3 <= data_i[31:24];
		    end
			  if (rsel[2] == 1'b1) begin
		      cache_get2 <= data_i[23:16];
		    end
		    if (rsel[1] == 1'b1) begin
		      cache_get1 <= data_i[15:8];
		    end
			  if (rsel[0] == 1'b1) begin
		      cache_get0 <= data_i[7:0];
		    end
		    end
		    1'b1:begin
		    if (rsel[3] == 1'b1) begin
		      cache_get7 <= data_i[31:24];
		    end
			  if (rsel[2] == 1'b1) begin
		      cache_get6 <= data_i[23:16];
		    end
		    if (rsel[1] == 1'b1) begin
		      cache_get5 <= data_i[15:8];
		    end
			  if (rsel[0] == 1'b1) begin
		      cache_get4 <= data_i[7:0];
		    end
		    end
		    endcase
	  end else if(rce == `ChipEnable) begin
	  		vce <= 1'b1;
	  		cache_get0 <= data_mem0[{raddr[`DataMemNumLog2+1:3], 1'b0}];
	  		cache_get1 <= data_mem1[{raddr[`DataMemNumLog2+1:3], 1'b0}];
	  		cache_get2 <= data_mem2[{raddr[`DataMemNumLog2+1:3], 1'b0}];
	  		cache_get3 <= data_mem3[{raddr[`DataMemNumLog2+1:3], 1'b0}];
	  		cache_get4 <= data_mem0[{raddr[`DataMemNumLog2+1:3], 1'b1}];
	  		cache_get5 <= data_mem1[{raddr[`DataMemNumLog2+1:3], 1'b1}];
	  		cache_get6 <= data_mem2[{raddr[`DataMemNumLog2+1:3], 1'b1}];
	  		cache_get7 <= data_mem3[{raddr[`DataMemNumLog2+1:3], 1'b1}];
		end else begin
				 
		end
	end		

endmodule